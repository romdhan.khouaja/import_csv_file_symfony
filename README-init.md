bundel de csv
ref: https://csv.thephpleague.com/9.0/
composer require league/csv

traitement des fichiers à charger 
ref: https://symfony.com/doc/master/bundles/EasyAdminBundle/integration/vichuploaderbundle.html
composer require vich/uploader-bundle

traitement des translators et datetimes
ref: https://symfony.com/doc/master/bundles/StofDoctrineExtensionsBundle/index.html
composer require stof/doctrine-extensions-bundle

traitement csv:
ref: https://codereviewvideos.com/course/how-to-import-a-csv-in-symfony
ref: https://symfony.com/doc/current/console.html
ref: https://packagist.org/packages/php-util/ansi-to-html-cli
ref: https://csv.thephpleague.com/

traitement des commandes sous symfony 
ref: https://symfony.com/doc/current/console/command_in_controller.html  
composer require sensiolabs/ansi-to-html

composer require twig/extensions

traitement de easy admin 
composer require admin

traitement utilisateur 
php bin/console make:user
php bin/console make:auth
php bin/console make:registration-form


traitement de l affichage response 
composer require php-util/ansi-to-html-cli

php bin/console make:entity DocumentCsv

fichiés à configurer : 

	configuration vich_uploader_bundle:
		vich_uploader.yaml
		services.yaml
		
	configuration stof_doctrine_extension:
		doctrine.yaml
		stof_doctrine_extensions.yaml
		


    orm:
        auto_generate_proxy_classes: '%kernel.debug%'
#        naming_strategy: doctrine.orm.naming_strategy.underscore
#        auto_mapping: true
        entity_managers:
            default:
#               connection: default
                naming_strategy: doctrine.orm.naming_strategy.underscore
                auto_mapping: true
                mappings:
                    App:
                        is_bundle: false
                        type: annotation
                        dir: '%kernel.project_dir%/src/Entity'
                        prefix: 'App\Entity'
                        alias: App
                    gedmo_translatable:
                        type: annotation
                        prefix: Gedmo\Translatable\Entity
                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translatable/Entity"
                        alias: GedmoTranslatable # (optional) it will default to the name set for the mapping
                        is_bundle: false
                    gedmo_translator:
                        type: annotation
                        prefix: Gedmo\Translator\Entity
                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Translator/Entity"
                        alias: GedmoTranslator # (optional) it will default to the name set for the mapping
                        is_bundle: false
#                    gedmo_loggable:
#                        type: annotation
#                        prefix: Gedmo\Loggable\Entity
#                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Loggable/Entity"
#                        alias: GedmoLoggable # (optional) it will default to the name set for the mappingmapping
#                        is_bundle: false
                    gedmo_tree:
                        type: annotation
                        prefix: Gedmo\Tree\Entity
                        dir: "%kernel.root_dir%/../vendor/gedmo/doctrine-extensions/lib/Gedmo/Tree/Entity"
                        alias: GedmoTree # (optional) it will default to the name set for the mapping
                        is_bundle: false


