<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Product; 
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface; 
use Symfony\Component\OptionsResolver\OptionsResolver; 

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('description') 
            ->add('category', EntityType::class,[
                'label'=>'Category',
                'class' => Category::class, 
                // uses the Category.name property as the visible option string
                'choice_label' => 'name',  
                'choice_value' => function (Category $entity = null) {
                    return $entity ? $entity->getId() : '';
                }, 
                ])  
            ->add('save',SubmitType::class,[
                'label'=>'Save',
                'attr'=>[
                    'class'=>'btn btn-success'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
