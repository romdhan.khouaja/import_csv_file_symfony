<?php

namespace App\Repository;

use App\Entity\DocumentCsv;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DocumentCsv|null find($id, $lockMode = null, $lockVersion = null)
 * @method DocumentCsv|null findOneBy(array $criteria, array $orderBy = null)
 * @method DocumentCsv[]    findAll()
 * @method DocumentCsv[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DocumentCsvRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DocumentCsv::class);
    }

    // /**
    //  * @return DocumentCsv[] Returns an array of DocumentCsv objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DocumentCsv
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
