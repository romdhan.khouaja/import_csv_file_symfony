<?php

namespace App\Controller;

use Twig\Environment;
use App\Entity\Category;
use App\Form\CategoryType;
use Psr\Log\LoggerInterface;
use App\Repository\CategoryRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route; 
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/category")
 */
class CategoryController extends AbstractController
{
    private $nav;
    private $session;
    private $logger;
    public function __construct($nav='category', SessionInterface $session, LoggerInterface $logger)
    {
        $this->session=$session;
        $this->logger = $logger;
        $this->nav = $nav;
    } 
 
    /**
     * @Route("/", name="category_index", methods={"GET","POST"}) )
     */
    public function index( CategoryRepository $categoryRepository, Request $request) 
    {  
        $this->logger->info('You\'re in index category controller !');
        return $this->render('category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
            // 'category' => $category,
            // 'form' => $form->createView(),
            'nav' => $this->nav,  
        ]);
    }
    
    
    /**
     * @Route("/new", name="category_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();

            return $this->redirectToRoute('category_index');
        }

        $content = $this->renderView('category/_form.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'nav' => $this->nav,
        ]);
        $response = new Response();
        $response->setContent($content);
        $response->setStatusCode(Response::HTTP_OK);
        // sets a HTTP response header
        $response->headers->set('Content-Type', 'text/html');
        // prints the HTTP headers followed by the content
        // $response->send(); 
        return $response;

    }

    /**
     * @Route("/{id}", name="category_show", methods={"GET"})
     */
    public function show(Category $category): Response
    {
        $content = $this->renderView('category/_show_modal.html.twig', [
            'category' => $category,
            'nav' => $this->nav,
        ]);
        
        $response = new Response();
        $response->setContent($content);
        $response->setStatusCode(Response::HTTP_OK);
        // sets a HTTP response header
        $response->headers->set('Content-Type', 'text/html');
        // prints the HTTP headers followed by the content
        // $response->send(); 
        return $response;
    }

    /**
     * @Route("/{id}/edit", name="category_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Category $category): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('category_index');
        }

        $content = $this->renderView('category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView(),
            'nav' => $this->nav,
        ]);
        $response = new Response();
        $response->setContent($content);
        $response->setStatusCode(Response::HTTP_OK);
        // sets a HTTP response header
        $response->headers->set('Content-Type', 'text/html');
        // prints the HTTP headers followed by the content
        // $response->send(); 
        return $response;

    }

    /**
     * @Route("/{id}", name="category_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Category $category): Response
    {
        if ($this->isCsrfTokenValid('delete'.$category->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('category_index');
    }
}
