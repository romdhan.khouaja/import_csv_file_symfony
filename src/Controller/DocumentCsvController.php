<?php

namespace App\Controller;

use App\Entity\DocumentCsv;
use App\Form\DocumentCsvType;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\DocumentCsvRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Routing\Annotation\Route;
use SensioLabs\AnsiConverter\AnsiToHtmlConverter;
use Symfony\Component\HttpKernel\KernelInterface;
use SensioLabs\AnsiConverter\Theme\SolarizedTheme;
use Symfony\Component\Console\Output\BufferedOutput; 
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Console\Application; 
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/documentcsv")
 */
class DocumentCsvController extends AbstractController
{
    private $session;
    private $nav;
    public function __construct($nav='document',SessionInterface $session)
    {
        $this->session = $session;
        $this->nav = $nav;
    }
     
    /**
    * @Route("/send/{file_name}", name="send_document_csv", methods={"GET","POST"})
    */
    public function sendSpool(EntityManagerInterface $em, KernelInterface $kernel, string $file_name="")
    {
        $application = new Application($kernel);
        $application->setAutoExit(false);
        // dd($file_name);
        $input = new ArrayInput([
            'command' => 'cvs:import:to:datatabase',
            // (optional) define the value of command arguments
            'arg1' => $file_name,
            // (optional) pass options to the command
            // '--message-limit' => $messages,
        ]);
        // dd($input);
        // You can use NullOutput() if you don't need the output
        $output = new BufferedOutput(
            OutputInterface::VERBOSITY_NORMAL,
            true // true for decorated
        );
        $application->run($input, $output);

        // return the output, don't use if you used NullOutput()
        $theme = new SolarizedTheme();
        $converter = new AnsiToHtmlConverter($theme, false); 
        $styles = $theme->asCss();
        $content = $output->fetch();
        // dd($converter->convert($content));
        // return new Response(""), if you used NullOutput()
        // $this->addFlash('success', (string) ());
        // return new Response($converter->convert($content));
        $this->session->set('message', $converter->convert($content));
        $this->session->set('mystyle', $styles);
         
        return $this->redirectToRoute('document_csv_index', [  
            'nav'       => $this->nav,
        ]);
    }
    
    /**
     * @Route("/", name="document_csv_index", methods={"GET"})
     */
    public function index(DocumentCsvRepository $documentCsvRepository): Response
    {
        $message = $this->session->get('message');
        $mystyle = $this->session->get('mystyle');
        $this->session->clear();

        return $this->render('document_csv/index.html.twig', [
            'document_csvs' => $documentCsvRepository->findAll(),
            'nav' => $this->nav,
            'message' => '<div class="flash-notice">'.$message.'</div>',
            'mystyle' => $mystyle,
        ]);
    }

    /**
     * @Route("/new", name="document_csv_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $documentCsv = new DocumentCsv();
        $form = $this->createForm(DocumentCsvType::class, $documentCsv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($documentCsv);
            $entityManager->flush();

            return $this->redirectToRoute('document_csv_index');
        }

        return $this->render('document_csv/new.html.twig', [
            'document_csv' => $documentCsv,
            'form' => $form->createView(),
            'nav' => $this->nav,
        ]);
    }

    /**
     * @Route("/{id}", name="document_csv_show", methods={"GET"})
     */
    public function show(DocumentCsv $documentCsv): Response
    {
        return $this->render('document_csv/show.html.twig', [
            'document_csv' => $documentCsv,
            'nav' => $this->nav,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="document_csv_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, DocumentCsv $documentCsv): Response
    {
        $form = $this->createForm(DocumentCsvType::class, $documentCsv);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('document_csv_index');
        }

        return $this->render('document_csv/edit.html.twig', [
            'document_csv' => $documentCsv,
            'form' => $form->createView(),
            'nav' => $this->nav,
        ]);
    }

    /**
     * @Route("/{id}", name="document_csv_delete", methods={"DELETE"})
     */
    public function delete(Request $request, DocumentCsv $documentCsv): Response
    {
        if ($this->isCsrfTokenValid('delete'.$documentCsv->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($documentCsv);
            $entityManager->flush();
        }

        return $this->redirectToRoute('document_csv_index');
    }
}
