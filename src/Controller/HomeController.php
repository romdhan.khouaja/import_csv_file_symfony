<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    private $nav;
    public function __construct($nav='accueil')
    {
        $this->nav = $nav;
    }
    /**
     * @Route("/", name="app_accueil")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'title' => 'Accueil',
            'nav' => $this->nav,
        ]);
    }
}
