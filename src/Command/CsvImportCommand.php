<?php

namespace App\Command;

use League\Csv\Reader;
use App\Entity\Examen;  
use App\Entity\Niveau ;  
use App\Entity\Matiere ;  

use App\Entity\DocumentCsv;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption; 
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface; 
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;  
use SensioLabs\AnsiConverter\Bridge\Twig\AnsiExtension; 
use SensioLabs\AnsiConverter\Theme\SolarizedTheme;

class CsvImportCommand extends Command  
{
    protected $count_error_duplicate = 0 ;
    protected $count_error_file_duplicate = 0 ;
    protected $count_success_file = 0 ;
    protected $count_success = 0 ;
    protected static $defaultName = 'cvs:import:to:datatabase';
    private $params;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * CsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     *
     * @throws \Symfony\Component\Console\Exception\LogicException
     */
    /**
     * CsvImportCommand constructor.
     *
     * @param EntityManagerInterface $em
     * @param ParameterBagInterface $params
     */
    public function __construct(EntityManagerInterface $em,ParameterBagInterface $params)
    { 
        $this->em = $em;
        $this->params = $params;
        parent::__construct();
        $this->count_error_duplicate = 0;
        $this->count_success = 0; 
        $this->count_error_file_duplicate = 0;
        $this->count_success_file = 0 ;
    }

    protected function configure()
    {
        $this
            ->setDescription('Creates a new import for csv file.')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }
    // fonction permettant de traiter le chemin dans windows rendre les / en \ 
    private function getCheminFichier(string $chaine){
        $app_path_csv_documents = $this->params->get('app.path.csv_documents');
        $kernel_project_dir = $this->params->get('kernel.project_dir');
        $resulat_chemin = $kernel_project_dir.'/public'.$app_path_csv_documents.'/'.$chaine ;
        // $dd = str_replace('\’', '\'', "String’s Title");
        $chemin = str_replace('/', '\\', $resulat_chemin);
        return $chemin;
    }
    
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        // $io->title('Importation des données ...<br>');
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            //  $io->note(sprintf('...Fichier nommé : %s <br>', $arg1));
            $chemin = self::getCheminFichier($arg1);
        }

        if ($input->getOption('option1')) {
            // aucune option à placer 
        }
        $document = $this->em->getRepository(DocumentCsv::class)->findOneBy(['csvName' => $arg1]); 
        if( null === $document || $document->getIsSent()===true){
            $io->error('Document a déja été envoyé ou est inexistant');
        }
        else
        { 
            // ############################################### 
            $reader = Reader::createFromPath($chemin,'r');  
            $reader->setDelimiter(';');
            $reader->setHeaderOffset(0);   

            $input_bom = $reader->getInputBOM(); 
            if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
                $reader->addStreamFilter('convert.iconv.UTF-16/UTF-8');
            }
            // traitement filtrage tableau du fichier
            $tableau_unique = array();
            foreach ($reader as $row) {
                if(in_array($row, $tableau_unique)){ 
                    // echo "ROW WAS FOUND\n";
                    $this->count_error_file_duplicate++;
                }else{
                    $this->count_success_file++;
                    // echo "row was not found\n";
                    // affectation du tableau non dupliqué 
                    $tableau_unique[] = $row; 
                } 
            } 
            // $io->progressStart(iterator_count($reader));
            //  foreach ($tableau_unique as $tab) {
            // var_dump($row);
            // $io->note(sprintf(var_dump($row)));
            // schema de correspondance csv : id_niveau;code_matiere
            foreach($tableau_unique as $row)
            {
                $code_niveau = $row["id_niveau" ];
                $niveau = $this->em->getRepository(Niveau::class)->findOneBy(['name' => $code_niveau]);
                if (!$niveau) 
                {
                    throw $this->createNotFoundException( 'Pas de niveau trouvé pour name '.$code_niveau );
                }

                $code_matiere = $row['code_matiere'];
                $matiere = $this->em->getRepository(Matiere::class)->findOneBy(['code' => $code_matiere]);
                if (!$matiere) 
                {
                    throw $this->createNotFoundException( 'Pas de matiere trouvée pour l\'id '.$code_matiere );
                }

                // faire une boucle pour trouver si l'examen existe pour eviter les duplications
                $examenResult = null;
                if ($matiere && $niveau){
                    $examenResult = $this->em->getRepository(Examen::class)
                    ->findOneBy([
                        'matiere' => $matiere,
                        'niveau' => $niveau,
                    ]);
                }
                if($examenResult !== null){
                    $this->count_error_duplicate++;
                }
                if ($examenResult === null) 
                {
                    $examen = new Examen();
                    $examen->setMatiere($matiere);
                    $examen->setNiveau($niveau) ; 

                    $this->em->persist($examen);
                    $this->count_success++;
                }
                // $io->progressAdvance();
            }
            //}
                
            // end of foreach
            // enregistrement total dans la db hors de la boucle  

            // test var_dump($document);
            // desactivation du document apres son importation 
            if($document)
            {
                $document->setIsSent(true);
                $this->em->persist($document);
                $this->em->flush();
            }

            if($this->count_success_file > 0){
                $io->success('Nombre de lignes accepté depuis le fichier = '.$this->count_success_file);
            }
            if($this->count_error_file_duplicate>0){
                $io->warning('Nombre de lignes dupliquées depuis le fichier = '.$this->count_error_file_duplicate);
            }

            if($this->count_success > 0){
                $io->success('Succes ! nombre de ligne acceptées pour l\'enregistrement = '.$this->count_success);
            }
            if($this->count_error_duplicate>0){
                $io->warning('Erreur ! nombre duplication dans la base de donnée = '.$this->count_error_duplicate);
            }
            
            // $io->progressFinish();
            
        }
    }
        
}
